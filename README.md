Tinyranosaur
===================

[https://tinyranosaur.firebaseapp.com](https://tinyranosaur.firebaseapp.com)

Your are a tyranosaur. But you are so small. Run for your life !

### Libraries used
* ThreeJS
* Lodash

### Installation

All you need is npm.

```sh
$ npm install -g grunt bower
$ npm install
```

### Running

```sh
$ grunt serve
```

### TODO

* Environnement
* Multiplayer interface to firebase
* Gameplay
* Intro scene
* Bundling
* Poo to slow down ennemies
* Move players with animations
